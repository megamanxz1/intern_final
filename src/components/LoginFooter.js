// Import scss files
import { Col, Row } from 'antd';
import 'antd/dist/antd.css';
import "../scss/login.scss";
// Import images
import Logo from "../assets/img/Logo.png";

function LoginFooter() {
    return ( <Row className="footer">
    <Col span={2} xxl={{span: 2}}><img src={Logo} alt='login_footer_logo'/></Col>
    <Col span={6} offset={1} xxl={{span: 5}}>
      <p className="footer__about">Là đơn vị uy tín và chuyên nghiệp trong hoạt động vận chuyển hàng hoá. Cung cấp cho khách hàng những dịch vụ vận chuyển tốt nhất</p>
      <p className="footer__about">Địa chỉ: 276 Huỳnh Ngọc Huệ, Phường Hoà Khê, Quận Thanh Khê, Đà Nẵng</p>
      <p className="footer__about">Website: <a href="/website">www.taobaodanang.com</a></p>
      <p className="footer__about">Email: <a href="/mail">taobaodanang@gmail.com</a></p>
    </Col>
    <Col span={5} offset={3}>
      <h2>Giới thiệu</h2>
      <ul>
        <li><a href="/abouts">Về Báo Đen Logistic</a></li>
        <li><a href="/services">Dịch vụ</a></li>
        <li><a href="/services">Tin tức</a></li>
        <li><a href="/contacts">Liên hệ</a></li>
      </ul>
    </Col>
    <Col span={6} offset={1}>
      <h2>Bộ phận chăm sóc khách hàng</h2>
      <ul>
        <li>CSKH 1: 0905 981 243 </li>
        <li>CSKH 2: 0908 301 243</li>
        <li>CSKH 3: 0903 651 243</li>
        <li>Kho Bãi: 0901 991 243</li>
        <li>Xử lý đơn hàng: 0906 461 243</li>
      </ul>
    </Col>
  </Row> );
}

export default LoginFooter;