import React from "react";
import { useState } from "react";
// Import Axios
import axios from "axios";
// Import components
import LoginHeader from "./LoginHeader";
import LoginFooter from "./LoginFooter";
import { Link, useNavigate } from "react-router-dom";
import { successNotification, failedNotification } from "./Utils";
// Import scss files
import { Row } from "antd";
import "antd/dist/antd.css";
import "../scss/login.scss";
// Import font awesome
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCircleUser,
  faLock,
  faEnvelope,
  faFileSignature,
  faPhone,
} from "@fortawesome/free-solid-svg-icons";
import "../scss/registerForm.scss";

function RegisterPage(props) {
  // Object stuff
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");
  const [inputPassword, setInputPassword] = useState("");
  const [matchPassword, setMatchPassword] = useState("");

  // Mockoon API
  const apiRegister = 'http://localhost:3004/register';

  // email regex
  const regex =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  // Navigate
  const navigate = useNavigate();

  // handle onClick
  const handleSubmit = (e) => {
    e.preventDefault();
    //email
    if (!regex.test(email)) {
      failedNotification("Email không đúng định dạng");
      return;
    }
    //password
    if (!/^.{8,}$/.test(inputPassword)) {
      failedNotification("Password phải dài từ 8 kí tự trở lên");
      return;
    }
    //matchPassword
    if (inputPassword !== matchPassword) {
      failedNotification("Password không trùng khớp");
      return;
    }
    axios.post(apiRegister, {username: username, email: email, name: name, phone: phone, password: inputPassword})
    .then(function (response) {
      // console.log(response.data); // Use to check data
      localStorage.setItem('AccessToken', `${response.data.data.token}`); // cant read
      // console.log(localStorage.getItem('AccessToken')); // Check token
      successNotification("Đăng kí thành công!.");
      // Send user info to home page?
      props.toggleIsLogged();
      navigate('/');
    })
    .catch(function (error) {
      // devide error status here to check is that account exist or email exist
      console.log(error);
      console.log('Errors: ',error.response.data.errors); //Fix here
      console.log('Status: ',error.response.data.status);
      failedNotification("Tài khoản đã tồn tại");
    });
  };
  
  return (
    <div>
      <LoginHeader />
        <Row className="content">
          <Row justify="center" align="middle">
            <div className="registerForm">
              <h1>Đăng Ký</h1>
              <form onSubmit={handleSubmit}>
                <div className="form-group">
                  <div>
                    <FontAwesomeIcon icon={faCircleUser} />
                  </div>
                  <input
                    type="text"
                    placeholder="Tên đăng nhập"
                    required
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                    className="Input"
                  />
                </div>
                <div className="form-group">
                  <div>
                    <FontAwesomeIcon icon={faEnvelope} />
                  </div>
                  <input
                    type="text"
                    placeholder="Email"
                    required
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    className="Input"
                  />
                </div>
                <div className="form-group">
                  <div>
                    <FontAwesomeIcon icon={faFileSignature} />
                  </div>
                  <input
                    type="text"
                    placeholder="Họ và tên"
                    required
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                    className="Input"
                  />
                </div>
                <div className="form-group">
                  <div>
                    <FontAwesomeIcon icon={faPhone} />
                  </div>
                  <input
                    type="text"
                    placeholder="Số điện thoại"
                    required
                    value={phone}
                    onChange={(e) => setPhone(e.target.value)}
                    className="Input"
                  />
                </div>
                <div className="form-group">
                  <div>
                    <FontAwesomeIcon icon={faLock} />
                  </div>
                  <input
                    type="password"
                    placeholder="Mật Khẩu"
                    required
                    value={inputPassword}
                    onChange={(e) => setInputPassword(e.target.value)}
                    className="Input"
                  />
                </div>
                <div className="form-group">
                  <div>
                    <FontAwesomeIcon icon={faLock} />
                  </div>
                  <input
                    type="password"
                    placeholder="Xác nhận lại mật khẩu"
                    required
                    value={matchPassword}
                    onChange={(e) => setMatchPassword(e.target.value)}
                    className="Input"
                  />
                </div>
                <button type="submit">Đăng Ký</button>
                <div className="loginForm__question">
                  <p>
                    Bạn đã có tài khoản?<Link to="/login"> Đăng Nhập</Link>
                  </p>
                </div>
              </form>
            </div>
          </Row>
        </Row>
      <LoginFooter />
    </div>
  );
}

export default RegisterPage;
