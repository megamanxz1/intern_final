import React,{ useContext } from "react";
// import { useRef } from "react"; //Fixing
import { useState } from "react";
// Import components
import LoginHeader from "./LoginHeader";
import LoginFooter from "./LoginFooter";
import { Link , useNavigate} from "react-router-dom";
import { IsLoggedContext } from "../App";
// Import scss files
import { Row } from "antd";
import "antd/dist/antd.css";
import "../scss/login.scss";
import "../scss/loginForm.scss";

// Import font awesome
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleUser, faLock } from "@fortawesome/free-solid-svg-icons";
import { successNotification, failedNotification } from "./Utils";
// Import Axios
import axios from "axios";

function LoginPage() {
  // Login authentication
  const isLogged = useContext(IsLoggedContext)
  //Bao Den
  // const apiLogin = 'https://baoden.tekup.vn/api/v2/auth/login';
  // Mockoon API
  const apiLogin = 'http://localhost:3004/login';

  // Object stuff
  const [usernameOrEmail, setUsernameOrEmail] = useState("");
  const [inputPassword, setInputPassword] = useState("");

  // Navigate
  const navigate = useNavigate();

  // onClicks, handles
  const handlesubmit = (e) => {
    e.preventDefault();
    // Axios post
    axios.post(apiLogin, {username: usernameOrEmail, password: inputPassword})
      .then(function (response) {
        // console.log(response.data); // Use to check data
        localStorage.setItem('AccessToken', `${response.data.data.token}`);
        // console.log(localStorage.getItem('AccessToken')); // Check token
        successNotification("Login successfuly directing you to the page.");
        isLogged() // useContext, Switch isLogged to true to gain access to Home Page
        navigate('/');
      })
      .catch(function (error) {
        console.log(error);
        failedNotification("Account or password is wrong!");
      });
  };

  return (
    <div>
      <LoginHeader />
      <Row className="content">
        <Row justify="center" align="middle">
          <div className="loginForm">
            <h1>Đăng Nhập</h1>
            <form onSubmit={handlesubmit}>
              <div className="form-group">
                <div>
                  <FontAwesomeIcon icon={faCircleUser} />
                </div>
                <input
                  type="text"
                  placeholder="Tên đăng nhập / Email"
                  required
                  value={usernameOrEmail}
                  onChange={(e) => setUsernameOrEmail(e.target.value)}
                  className="Input"
                />
              </div>
              <div className="form-group">
                <div>
                  <FontAwesomeIcon icon={faLock} />
                </div>
                <input
                  type="Password"
                  placeholder="Mật Khẩu"
                  required
                  value={inputPassword}
                  onChange={(e) => setInputPassword(e.target.value)}
                  className="Input"
                />
              </div>
              <div className="loginForm__checkbox">
                <p>
                  <input type="checkbox"></input>
                  <span>Ghi nhớ đăng nhập</span>
                </p>
                <Link to="/forgot">Quên Mật Khẩu?</Link>
              </div>
              <button type="submit">Đăng Nhập</button>
              <div className="loginForm__question">
                <p>
                  Bạn chưa có tài khoản?{" "}
                  <Link to="/register">Đăng ký ngay</Link>
                </p>
              </div>
            </form>
          </div>
        </Row>
      </Row>
      <LoginFooter />
    </div>
  );
}

// Export component to other files to use
export default LoginPage;