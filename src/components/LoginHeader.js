// Import Router
import { Link } from "react-router-dom";
import { useLocation } from 'react-router-dom';
// Import scss files
import { Col, Row } from 'antd';
import 'antd/dist/antd.css';
import "../scss/login.scss";
// Import images
import Logo from "../assets/img/Logo.png";

function LoginHeader() {
  const location = useLocation(); 
  return (
    <Row className='header' align='middle'>
      <Col xs={{span: 6}} sm={{span: 6}} md={{span: 6}} lg={{span: 5}} xl={{span: 4}} xxl={{span: 2}} >
        <img src={Logo} alt='login_header_logo' />
      </Col>
      <Col className='header__rate' xs={{span: 5, offset: 0}} sm={{span: 5, offset: 0}} md={{span: 6, offset: 0}} lg={{span: 6, offset: 1}} xl={{span: 4, offset: 1}} xxl={{span: 3, offset: 1}}>
        Tỷ giá hôm nay:<span> 3.740đ</span>
      </Col>
      <Col xs={{span: 12,offset: 1}} sm={{span: 12,offset: 1}} md={{span: 10, offset: 2}} lg={{span: 7, offset: 5}} xl={{span: 6,offset: 9}} xxl={{span: 4, offset: 14}} >
        <Row justify='space-evenly'>
          <Link className={(location.pathname) === '/register'?'switchButton':''} to='/register'>
            <button>Đăng Ký</button>
          </Link>
          <Link className={(location.pathname) === '/login'?'switchButton':''} to='/login'>
            <button>Đăng Nhập</button>
          </Link>
        </Row>
      </Col>
    </Row>
  );
}

export default LoginHeader;
