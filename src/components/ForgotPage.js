import React, { useState } from "react";
// Import components
import LoginHeader from "./LoginHeader";
import LoginFooter from "./LoginFooter";
import { Link } from "react-router-dom";
import { successNotification, failedNotification } from "./Utils";
// Import Axios
import axios from "axios";
// Import scss files
import { Row } from "antd";
import "antd/dist/antd.css";
import "../scss/login.scss";
// Import font awesome
import "../scss/forgotForm.scss";

function ForgotPage() {
  
  const [recoverDetail, setRecoverDetail] = useState("");
  // email regex 10 numbers phone or email format
  const regex = /^(?:\d{10}|\w+@\w+\.\w{2,3})$/;
  // Mockoon API
  const apiForgot = 'http://localhost:3004/forgot';

  const handleRecover = (e) => {
    e.preventDefault();
    // regex
    if (!regex.test(recoverDetail)) {
      failedNotification("Email hoặc số điện thoại không hợp lệ!");
      return;
    }

    axios.post(apiForgot, {email: recoverDetail})
    .then(function (response) {
      // console.log(response.data); // Use to check data
      // Send recovery to mail
      successNotification("Quý khách vui lòng kiểm tra hộp thư để khôi phục mật khẩu");
      // Send to user Email the link to get password
    })
    .catch(function (error) {
      // devide error status here to check is that account exist or email exist
      console.log(error.response.data.status);
      failedNotification("Tài khoản không tồn tại");
    });
  };

  return (
    <div>
      <LoginHeader />
      <Row className="content">
        <Row justify="center" align="middle">
          <div className="forgotForm">
            <h1>Quên mật khẩu</h1>
            <form onSubmit={handleRecover}>
              <h3>Nhập email hoặc số điện thoại để lấy lại mật khẩu</h3>
              <input
                type="text"
                placeholder="Email hoặc số điện thoại"
                required
                value={recoverDetail}
                onChange={(e) => setRecoverDetail(e.target.value)}
                className="Input"
              />
              <button type="submit">Khôi phục</button>
              <div className="forgotForm__question">
                <p>
                  Bạn chưa có tài khoản?<Link to="/register"> Đăng Ký</Link>
                </p>
              </div>
            </form>
          </div>
        </Row>
      </Row>
      <LoginFooter />
    </div>
  );
}

export default ForgotPage;
