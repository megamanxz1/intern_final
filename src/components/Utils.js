import { notification } from 'antd';
import { CheckCircleOutlined, CloseSquareOutlined } from '@ant-design/icons';

// Validate Notification (Description)
  const successNotification = (descMsg) => {
    notification.info({
      placement: 'topRight',
      message: 'Success',
      description: descMsg,
      duration: 3,
      icon: (<CheckCircleOutlined style={{color: '#33cc33',}}/>),
    });
  }
  //
  const failedNotification = (descMsg) => {
    notification.info({
      placement: 'topLeft',
      message: 'Failed',
      description: descMsg,
      duration: 3,
      icon: (<CloseSquareOutlined style={{color: '#ff0000',}}/>),
    });
  }

export { successNotification, failedNotification }
