import React from "react";
// Import images
import Logo from "../assets/img/Logo.png";
// Import Css
import { Col, Row } from "antd";
import "antd/dist/antd.css";
import '../scss/home.scss'
// Import Fontawesome
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleDown, faClipboard, faTruckFast, faCircleDollarToSlot, faBitcoinSign, faFileExport, faCircleInfo } from "@fortawesome/free-solid-svg-icons";

function HomePage() {

    /** Issues
     *  
     */

    return ( 
        <div>
            <Row className="homePage" >
                <Col span={5} className='homePage__nav'> 
                    <Row justify="center" align="middle"><img className="homePage__nav__logo" src={Logo} alt=''/></Row>
                    <Row className="homePage__nav__Row" align="middle"><FontAwesomeIcon className="homePage__nav__Row__iconLeft" icon={faClipboard} /><span>Quản lý đơn hàng</span><FontAwesomeIcon className="homePage__nav__Row__iconRight" icon={faAngleDown} /></Row>
                    <Row className="homePage__nav__Row" align="middle"><FontAwesomeIcon className="homePage__nav__Row__iconLeft" icon={faTruckFast} /><span>Quản lý giao hàng</span><FontAwesomeIcon className="homePage__nav__Row__iconRight" icon={faAngleDown} /></Row>
                    <Row className="homePage__nav__Row" align="middle"><FontAwesomeIcon className="homePage__nav__Row__iconLeft" icon={faCircleDollarToSlot} /><span>Quản lý ký gửi</span><FontAwesomeIcon className="homePage__nav__Row__iconRight" icon={faAngleDown} /></Row>
                    <Row className="homePage__nav__Row" align="middle"><FontAwesomeIcon className="homePage__nav__Row__iconLeft" icon={faBitcoinSign} /><span>Quản lý ví tiền</span><FontAwesomeIcon className="homePage__nav__Row__iconRight" icon={faAngleDown} /></Row>
                    <Row className="homePage__nav__Row" align="middle"><FontAwesomeIcon className="homePage__nav__Row__iconLeft" icon={faFileExport} /><span>Xuất báo cáo</span><FontAwesomeIcon className="homePage__nav__Row__iconRight" icon={faAngleDown} /></Row>
                    <Row className="homePage__nav__Row" align="middle"><FontAwesomeIcon className="homePage__nav__Row__iconLeft" icon={faCircleInfo} /><span>Thông tin khác</span><FontAwesomeIcon className="homePage__nav__Row__iconRight" icon={faAngleDown} /></Row>
                </Col>
                <Col span={19}>
                    <h1>Home Page</h1>
                </Col>
            </Row>
        </div>
    )
}

export default HomePage