import React, { createContext } from "react";
import { useState } from "react";

// Import components
import HomePage from "./components/HomePage";
import LoginPage from "./components/LoginPage";
import RegisterPage from "./components/RegisterPage";
import ForgotPage from "./components/ForgotPage";
//Import Router
import { Routes, Route, Navigate  } from "react-router-dom";

// Main App
function App() {

  // Check authenication
  const [isLogged, setIsLogged] = useState(false);
  const toggleIsLogged = () => {setIsLogged(!isLogged)}
  localStorage.setItem('AccessToken', '');
  
  return (
    <IsLoggedContext.Provider value={toggleIsLogged}>
      <Routes>
        <Route path="/" element={isLogged?<HomePage />:<Navigate to='login'/>} />
        <Route path="/login" element={<LoginPage />} />
        <Route path="/register" element={<RegisterPage toggleIsLogged={toggleIsLogged} />} />
        <Route path="/forgot" element={<ForgotPage />} />
      </Routes>
    </IsLoggedContext.Provider>
  );
}

export default App;
export const IsLoggedContext = createContext();